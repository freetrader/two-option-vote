import {
  Blockchain, BLANK_VOTE, calculateProposalID, tallyVotes,
  derivePublicKey, createSalt, hash160Salted, generatePrivateKey,
  getPublicKeyHash, TwoOptionVoteContract, TwoOptionVote,
} from '../lib';

/**
 * This example has 6 participants, where
 *
 * - 1 votes for option A
 * - 1 votes for option B
 * - 2 vote blank
 * - 1 does not vote.
 */

const sleep = require('sleep');

// Change address can be anything.
const FINAL_CHANGE_ADDRESS = 'bitcoincash:qr8p3mpz2gm9ykhj0k50fhtrxnfc87qs8yf7l3slzv';

function toHex(buffer: Uint8Array): string {
  return Buffer.from(buffer).toString('hex');
}

async function run(): Promise<void> {
  const chain = new Blockchain();
  if (!await chain.connect()) {
    throw Error('Electrum connection error');
  }

  // Set up voters
  const voters = Array.from({ length: 6 }, () => generatePrivateKey());

  const votersPKH: Buffer[] = [];
  for (const v of voters) {
    votersPKH.push(Buffer.from(
      await getPublicKeyHash(await derivePublicKey(v)),
    ));
  }

  // Let the vote end two blocks from now (in about 10 - 20 minutes)
  const endHeight = 2 + await chain.getBlockchainTipHeight();

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt: createSalt(),
    description: 'Foo?',
    optionA: 'Bar',
    optionB: 'Baz',
    endHeight,
    votersPKH,
  };

  // Show some output
  const [id, optionAHash, optionBHash] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);

  console.info(`Election salt ${toHex(election.salt)}`);
  console.info(`Election end height ${election.endHeight}`);
  console.info(`Election ID ${toHex(id)}`);
  console.info('Election option A: %s (%s)',
    election.optionA, toHex(optionAHash));
  console.info('Election option B: %s (%s)',
    election.optionB, toHex(optionBHash));
  console.info(`Blank vote is ${toHex(BLANK_VOTE)}`);

  const castVote = async (
    voterPrivateKey: Uint8Array,
    option: Uint8Array,
    changeAddress: string) => {
    const contract = await TwoOptionVoteContract.make(election, voterPrivateKey);
    const voterAddress = await contract.getVoterAddress();
    console.info(`Voter ${voterAddress} to vote for ${toHex(option)}`);
    console.info(`Contract address: ${contract.getContractAddress()}`);

    const mininmumBalance = 1000;

    await contract.waitForBalance(mininmumBalance, async (currentBalance) => {
      console.info(`Too low balance (${currentBalance} < ${mininmumBalance}), waiting...`);
    });

    const txid = await contract.castVote(option, changeAddress);
    console.info(`Success ${txid}`);
  };

  const getAddress = async (voterPrivateKey: Uint8Array) => {
    const contract = await TwoOptionVoteContract.make(election, voterPrivateKey);
    return contract.getContractAddress();
  };

  // Votes Option A
  await castVote(voters[0], optionAHash, await getAddress(voters[1]));

  // Votes for option B
  await castVote(voters[1], optionBHash, await getAddress(voters[2]));

  // Two blank votes
  await castVote(voters[2], BLANK_VOTE, await getAddress(voters[3]));
  await castVote(voters[3], BLANK_VOTE, await getAddress(voters[4]));

  // This voter invalidates his vote by voting twice
  await castVote(voters[4], optionAHash, await getAddress(voters[4]));
  await castVote(voters[4], optionBHash, FINAL_CHANGE_ADDRESS);

  // Finally, the last voter (contracts[5]) does not vote ...

  console.info('Waiting for transactions to propagate...');
  sleep.sleep(5);
  try {
    const tally = await tallyVotes(chain, election, true);
    console.log(`Result: ${JSON.stringify(tally, null, 4)}`);
  } finally {
    chain.disconnect();
  }
}

run().catch((err) => {
  console.warn(err);
});
