import assert from 'assert';
import { ElectrumClient } from 'electrum-cash';
import { decodeTransaction } from '@bitauth/libauth';
import { toOutputScript } from './transaction';

/**
 * We don't do SPV verification (yet?), so this needs to be a server that
 * does not lie to us.
 */
const SERVER_WE_TRUST = 'electrs.bitcoinunlimited.info';

function isOutputTo(output: any, addr: string): boolean {
  const script = toOutputScript(addr);
  return output.lockingBytecode.compare(script) === 0;
}

export default class Blockchain {
    electrum: any;

    server: string = SERVER_WE_TRUST;

    constructor(server?: string) {
      if (server !== undefined) {
        this.server = server;
      }
      this.electrum = null;
    }

    public async connect(): Promise<boolean> {
      if (this.electrum !== null) {
        return true;
      }

      this.electrum = new ElectrumClient('The best client', '1.4', this.server);
      return this.electrum.connect();
    }

    public disconnect() {
      assert(this.electrum !== null);
      this.electrum.disconnect();
    }

    public async hasInputFrom(tx: any, addr: string): Promise<boolean> {
      for (const input of tx.inputs) {
        const txhash = input.outpointTransactionHash.toString('hex');

        const inputTx: any = await this.getTx(txhash);
        if (isOutputTo(inputTx.outputs[input.outpointIndex], addr)) {
          return true;
        }
      }
      return false;
    }

    public async getTx(txid: string) {
      assert(this.electrum !== null);
      const txHex = await this.electrum.request(
        'blockchain.transaction.get', txid,
      );
      if (typeof (txHex) !== 'string') {
        throw txHex;
      }
      const tx: Buffer = Buffer.from(txHex, 'hex');

      return decodeTransaction(tx);
    }

    /**
     * Get transactions that _spend_ from address.
     *
     * @param addr - Bitcoin Cash address
     * @param maxheight - Filter out transactions confirmed after this height
     * @param includeUnconfirmed - Include unconfirmed (when maxheight > tip)
     */
    public async getSpendingTxs(addr: string, maxheight: number,
      includeUnconfirmed: boolean): Promise<any[]> {
      assert(this.electrum !== null);

      const history = await this.electrum.request(
        'blockchain.address.get_history', addr,
      );

      const tipheight = await this.getBlockchainTipHeight();

      const txs: any[] = [];
      for (const entry of history) {
        // Check if tx is unconfirmed.
        // * 0 means unconfirmed.

        // * -1 means unconfirmed with unconfirmed parent.
        if (entry.height <= 0) {
          if (!includeUnconfirmed) {
            continue;
          }
          if (tipheight >= maxheight) {
            // Above max height. Cannot include unconfirmed txs.
            continue;
          }
        }
        if (entry.height > maxheight) {
          continue;
        }
        const tx = await this.getTx(entry.tx_hash);
        if (await this.hasInputFrom(tx, addr)) {
          txs.push(tx);
        }
      }
      return txs;
    }

    public async getBlockchainTipHeight(): Promise<number> {
      const header = await this.electrum.request('blockchain.headers.subscribe');

      // We don't actually want events on new blocks. We just wanted the tip.
      // So unsubscribe immediately.
      try {
        await this.electrum.request('blockchain.headers.unsubscribe');
      } catch {
        // ignore error
      }

      return header.height;
    }

    public async ping(): Promise<void> {
      await this.electrum.request('server.ping');
    }
}
