import { tallyVotes } from './tally';
import { getExampleElection } from './testutil';
import Blockchain from './blockchain';

test('tallyVotes', async () => {
  jest.setTimeout(30000);
  const election = await getExampleElection();

  const chain = new Blockchain();
  await chain.connect();

  const result = await tallyVotes(chain, election, true);

  expect(result.optionA.length).toBe(1);
  expect(result.optionB.length).toBe(1);
  expect(result.blank.length).toBe(2);
  expect(result.noVote.length).toBe(1);
  expect(result.invalidVote.length).toBe(1);

  expect(result.optionA[0].voterPKH).toBe(election.votersPKH[0]);
  expect(result.optionB[0].voterPKH).toBe(election.votersPKH[1]);
  expect(result.blank[0].voterPKH).toBe(election.votersPKH[2]);
  expect(result.blank[1].voterPKH).toBe(election.votersPKH[3]);
  expect(result.invalidVote[0].voterPKH).toBe(election.votersPKH[4]);
  expect(result.noVote[0].voterPKH).toBe(election.votersPKH[5]);

  chain.disconnect();
});
