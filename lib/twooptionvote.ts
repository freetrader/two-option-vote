/* eslint class-methods-use-this: "off" */
/* eslint no-param-reassign: "off" */
import { SignatureTemplate } from 'cashscript';
import { hashToAddress, getTxID } from './transaction';
import { compileTwoOptionVote, calculateProposalID, createVoteMessage } from './primitives';
import {
  getPublicKeyHash, derivePublicKey, signMessageWithSchnorr, hash160Salted,
} from './crypto';
import { BLANK_VOTE, DEFAULT_CAST_TX_FEE, DUST } from './votedef';

const sleep = require('sleep');

export interface TwoOptionVote {
    readonly network: 'mainnet' | 'testnet';
    readonly salt: Uint8Array,
    readonly description: string,
    readonly optionA: string,
    readonly optionB: string,
    readonly votersPKH: Uint8Array[],
    readonly endHeight: number,
}

export class TwoOptionVoteContract {
    election: TwoOptionVote;

    voterPrivateKey: Uint8Array;

    contract: any;

    private constructor(election: TwoOptionVote,
      voterPrivateKey: Uint8Array) {
      this.election = election;
      this.voterPrivateKey = voterPrivateKey;
    }

    /**
      * Create a contract instance for a participating voter.
      *
      * @param election The election voter is participating in.
      * @param voterPrivateKey Private key used for signing the vote and the transaction itself.
      */
    public static async make(election: TwoOptionVote,
      voterPrivateKey: Uint8Array): Promise<TwoOptionVoteContract> {
      const instance = new TwoOptionVoteContract(election, voterPrivateKey);

      instance.contract = compileTwoOptionVote(
        election.network,
        await calculateProposalID(election),
        await instance.optionAHash(),
        await instance.optionBHash(),
        await instance.voterPKH(),
      );

      return instance;
    }

    /**
      * The bitcoin cash address for this contract.
      */
    public getContractAddress(): string {
      if (this.contract !== undefined) {
        return this.contract.address;
      }
      throw Error('invalid state');
    }

    /**
      * Wait for contract to contain at minimum this balance.
      *
      * @param minimumBalance Minimum balance before returning.
      * @param waitForBalance Called at intervals while waiting on balance.
      */
    public async waitForBalance(minimumBalance: number,
      waitCallback: (balance: number) => void): Promise<number> {
      if (this.contract === undefined) {
        throw Error('invalid state');
      }

      let balance = 0;
      /* eslint no-constant-condition: ["error", { "checkLoops": false }] */
      while (true) {
        balance = await this.contract.getBalance();
        if (balance >= 1000) {
          break;
        }
        await waitCallback(balance);
        sleep.sleep(2);
      }
      return balance;
    }

    /**
      * Creates a messaged + signature for message with signed vote.
      *
      * @param privatekey Private key of the particpant owning this contract.
      * @param option The option to vote on.
      */
    public async signVoteMessage(privatekey: Uint8Array,
      optionHash: Uint8Array): Promise<[Uint8Array, Uint8Array]> {
      const id = await calculateProposalID(this.election);
      const message = await createVoteMessage(id, optionHash);
      const signature = await signMessageWithSchnorr(privatekey, message);
      return [message, signature];
    }

    /**
      * The salted hash of vote option A.
      */
    public async optionAHash(): Promise<Uint8Array> {
      return hash160Salted(this.election.salt, Buffer.from(this.election.optionA));
    }

    /**
      * The salted hash of vote option B.
      */
    public async optionBHash(): Promise<Uint8Array> {
      return hash160Salted(this.election.salt, Buffer.from(this.election.optionB));
    }

    /**
      * The hash for blank vote.
      */
    public optionBlank(): Uint8Array {
      return BLANK_VOTE;
    }

    /**
      * Get P2PKH address of voter (not the contract address!)
      */
    public async getVoterAddress() {
      return hashToAddress(await this.voterPKH(), 'p2pkh');
    }

    /**
      * Create and submit a transaction that performs a vote.
      *
      * @param privatekey Private key of owner of the contract.
      * @param optionHash The option to vote for.
      * @param changeAddress Where to send contract coins to. Defaults to users P2PKH address.
      * @returns Transaction ID
      */
    public async castVote(optionHash: Uint8Array,
      changeAddress?: string,
      fee: number = DEFAULT_CAST_TX_FEE): Promise<string> {
      if (this.contract === undefined) {
        Error('invalid state');
      }

      if (changeAddress === undefined) {
        changeAddress = await this.getVoterAddress();
      }
      const publicKey = derivePublicKey(this.voterPrivateKey);

      const [message, messageSignature] = await this.signVoteMessage(
        this.voterPrivateKey, optionHash,
      );

      const txSignature = new SignatureTemplate(this.voterPrivateKey);

      const balance = await this.contract.getBalance();
      if (balance < fee + DUST) {
        throw Error('Insufficiant balance');
      }

      const tx = await this.contract.functions.cast_vote(await publicKey,
        txSignature, message, messageSignature)
        .to(changeAddress, balance - fee)
        .send()
        .catch((e: any) => {
          throw e;
        });

      return getTxID(tx);
    }

    /**
      * Get the public key hash of the voter this contract instance bleongs to.
      */
    public async voterPKH() {
      return getPublicKeyHash(await derivePublicKey(this.voterPrivateKey));
    }
}
