/* eslint max-classes-per-file: ["error", 2] */
/* eslint no-empty-function: "off" */
import InvalidVoteError from './error/invalidvoteerror';
import NoVoteError from './error/novoteerror';
import { BLANK_VOTE } from './votedef';
import { calculateProposalID, deriveContractAddress } from './primitives';
import { getTxID } from './transaction';
import { parseVoteFromTx, fetchVoteTx } from './voteverify';
import Blockchain from './blockchain';
import { TwoOptionVote } from './twooptionvote';
import { hash160Salted } from './crypto';

const assert = require('assert');

/**
 * An election vote retrieved from the blockchain.
 */
export interface Ballot {
    /**
     * Public Key Hash of voter
     */
  voterPKH: Uint8Array,
  /**
    * Transaction containing vote.
    * Is null on "no vote" or "invalid vote"
    */
  txid: string | null,
  /**
    * What ballot voted for
    * Is null on "no vote" or "invalid vote"
    */
  voteOption: Uint8Array | null,
}

/**
 * Collection of tallied votes for an election
 */
export interface VoteTally {
    /**
      * Votes for option A
      */
    optionA: Ballot[];

    /**
      * Votes for option B
      */
    optionB: Ballot[];

    /**
      * Votes for blank
      */
    blank: Ballot[];

    /**
      * Participants that did not vote.
      * In this case, both transactionID and vote are set to null.
      */
    noVote: Ballot[];

    /**
      * Participants that invalidated their vote.
      * For example by casting two votes at same block height.
      * In this case, both transactionID and vote are set to null.
      */
    invalidVote: Ballot[];
}

/**
 * Queries the blockchain for a full tally of a past or ongoing vote.
 *
 * @param chain Electrum connection
 * @param proposalID The election ID
 * @param optA Hash for option A
 * @param optB Hash for option B
 * @param votersPKH List of PKH for all participating voters.
 * @param endheight The blockheight the election ends
 * @param includeUnconfirmed Include unconfirmed ballots (only applicable if election has not ended)
 */
export async function tallyVotes(
  chain: Blockchain,
  election: TwoOptionVote,
  includeUnconfirmed: boolean,
): Promise<VoteTally> {
  const result: VoteTally = {
    optionA: [],
    optionB: [],
    blank: [],
    noVote: [],
    invalidVote: [],
  };

  const [proposalID, optionAHash, optionBHash] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);

  for (const voterPKH of election.votersPKH) {
    const address = await deriveContractAddress(proposalID,
      optionAHash, optionBHash,
      voterPKH);
    try {
      const tx = await fetchVoteTx(chain, address,
        election.endHeight, includeUnconfirmed);

      const voteOption = Buffer.from(parseVoteFromTx(
        tx, proposalID, optionAHash, optionBHash, voterPKH,
      ));

      const txid = await getTxID(tx);
      if (voteOption.compare(optionAHash) === 0) {
        result.optionA.push({ voterPKH, txid, voteOption });
      } else if (voteOption.compare(optionBHash) === 0) {
        result.optionB.push({ voterPKH, txid, voteOption });
      } else if (voteOption.compare(BLANK_VOTE) === 0) {
        result.blank.push({ voterPKH, txid, voteOption });
      } else {
        assert(false);
      }
    } catch (e) {
      if (e instanceof NoVoteError) {
        result.noVote.push({ voterPKH, txid: null, voteOption: null });
      } else if (e instanceof InvalidVoteError) {
        result.invalidVote.push({ voterPKH, txid: null, voteOption: null });
      } else {
        throw e;
      }
    }
  }
  return result;
}
