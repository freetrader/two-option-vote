import Blockchain from './blockchain';
import NoVoteError from './error/novoteerror';
import { createRandomAddress, getExampleElection } from './testutil';
import { calculateProposalID, deriveContractAddress } from './primitives';
import { fetchVoteTx, parseVoteFromTx } from './voteverify';
import { BLANK_VOTE } from './votedef';
import { hash160Salted } from './crypto';

test('fetch_vote_no_vote_cast_error', async () => {
  const contractAddr = createRandomAddress();

  const chain = new Blockchain();
  await chain.connect();

  const t = async () => {
    await fetchVoteTx(chain, contractAddr, 1337, true);
  };
  await expect(t()).rejects.toEqual(new NoVoteError());

  chain.disconnect();
});

test('parseVoteFromTx', async () => {
  jest.setTimeout(30000);
  const chain = new Blockchain();
  await chain.connect();

  const election = await getExampleElection();

  const [proposal, optA, optB] = await Promise.all([
    calculateProposalID(election),
    hash160Salted(election.salt, Buffer.from(election.optionA)),
    hash160Salted(election.salt, Buffer.from(election.optionB))]);

  const votes: Buffer[] = [];
  for (const voterpkh of election.votersPKH.slice(0, 3)) {
    const address = await deriveContractAddress(proposal, optA, optB, voterpkh);
    const tx = await fetchVoteTx(chain, address, election.endHeight, true);
    votes.push(Buffer.from(
      await parseVoteFromTx(tx, proposal, optA, optB, voterpkh),
    ));
  }
  expect(votes).toEqual([
    Buffer.from(optA),
    Buffer.from(optB),
    Buffer.from(BLANK_VOTE)]);

  chain.disconnect();
});

test('fetch_vote_multiple_spends_ok', async () => {
  // TODO: Add test with multiple spends at different height.
  // Test that the first one counts.
});

test('fetch_vote_multiple_spends_error', async () => {
  // TODO: Add test with multiple spends at same height,
  // verify that this spoils the vote.
});
