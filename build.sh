#!/usr/bin/env bash

# exit when any command fails
set -e

if [ ! -d "./node_modules" ]; then
    npm install
fi

#if [ ! -d "./node_modules/cashscript/node_modules/cramer-bch" ]; then
#    # Workaround for cashscript issue
#    (cd node_modules/cashscript/node_modules; ln -s bitcoincashjs-lib cramer-bch)
#fi

mkdir -p build/contracts
cp contracts/* build/contracts/

./node_modules/cashc/dist/cashc-cli.js --hex \
    ./contracts/two-option-vote.cash > ./build/contracts/two-option-vote.hex && \

npm run tsc --build tsconfig.json
